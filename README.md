**integration:**

use command: **docker-compose up** to start DBMS "Postgres"

**shell samples:**

- bookstore: add --title new --genre fairytale --name Awesome --surname Author
- bookstore: find --genre "adventure novel"
- bookstore: find --surname Verne
