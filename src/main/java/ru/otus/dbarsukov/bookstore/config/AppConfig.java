package ru.otus.dbarsukov.bookstore.config;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.shell.jline.PromptProvider;

@Configuration
public class AppConfig {
    @Bean
    public PromptProvider myPromptProvider() {
        return () -> new AttributedString("bookstore:>", AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
    }
}
