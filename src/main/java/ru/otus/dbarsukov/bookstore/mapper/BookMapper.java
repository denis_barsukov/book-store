package ru.otus.dbarsukov.bookstore.mapper;

import org.springframework.jdbc.core.RowMapper;
import ru.otus.dbarsukov.bookstore.domain.Author;
import ru.otus.dbarsukov.bookstore.domain.Book;
import ru.otus.dbarsukov.bookstore.domain.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {

    @Override
    public Book mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Book(resultSet.getLong("id"),
                resultSet.getString("title"),
                new Author(resultSet.getLong("author_id"),
                        resultSet.getString("name"),
                        resultSet.getString("surname")),
                new Genre(resultSet.getLong("genre_id"),
                        resultSet.getString("genre")));
    }
}
