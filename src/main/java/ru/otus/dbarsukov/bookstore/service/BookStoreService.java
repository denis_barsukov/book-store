package ru.otus.dbarsukov.bookstore.service;

import java.util.List;

public interface BookStoreService {
    String getInfo();
    List<String> getAllAuthors();
    List<String> getAllGenres();
    List<String> getAllBooks();
    List<String> find(String title, String genre, String name, String surname);
    void addBook(String title, String genre, String name, String surname);
}
