package ru.otus.dbarsukov.bookstore.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.dbarsukov.bookstore.dao.AuthorDao;
import ru.otus.dbarsukov.bookstore.dao.BookDao;
import ru.otus.dbarsukov.bookstore.dao.GenreDao;
import ru.otus.dbarsukov.bookstore.domain.Author;
import ru.otus.dbarsukov.bookstore.domain.Book;
import ru.otus.dbarsukov.bookstore.domain.Genre;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookStoreServiceImpl implements BookStoreService {
    private final AuthorDao authors;
    private final BookDao books;
    private final GenreDao genre;

    @Override
    public String getInfo() {
        return String.format("Books in store: %d, authors: %d",
                books.count(), authors.count());
    }

    @Override
    public List<String> getAllAuthors() {
        return authors.readAll()
                .stream()
                .map(author -> author.getName() + " " + author.getSurname())
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getAllGenres() {
        return genre.readAll()
                .stream()
                .map(Genre::getGenre)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getAllBooks() {
        return books.readAll()
                .stream()
                .map(book -> book.getTitle() +
                        " (" + book.getGenre().getGenre() + ")" +
                        " by " + book.getAuthor().getName() + " " + book.getAuthor().getSurname())
                .collect(Collectors.toList());
    }

    @Override
    public List<String> find(String title, String genre, String name, String surname) {
        return books.findByParams(title, genre, name, surname)
                .stream()
                .map(book -> book.getTitle() +
                        " (" + book.getGenre().getGenre() + ")" +
                        " by " + book.getAuthor().getName() + " " + book.getAuthor().getSurname())
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void addBook(String title, String genre, String name, String surname) {
        Long authorId = getAuthorIdOrCreate(name, surname);
        Long genreId = getGenreIdOrCreate(genre);
        Long bookId = books.create(new Book(null, title,
                new Author(authorId, null, null),
                new Genre(genreId, null)));
        log.debug("new bookId={}", bookId.toString());
    }

    private Long getAuthorIdOrCreate(String name, String surname) {
        Long authorId = authors.findIdByParams(name, surname);
        log.debug("found authorId={}", authorId.toString());
        if (authorId == -1) {
            authorId = authors.create(new Author(null, name, surname));
            log.debug("new authorId={}", authorId.toString());
        }
        return authorId;
    }

    private Long getGenreIdOrCreate(String genre) {
        Long genreId = this.genre.findIdByParams(genre);
        log.debug("found genreId={}", genreId.toString());
        if (genreId == -1) {
            genreId = this.genre.create(new Genre(null, genre));
            log.debug("new genreId={}", genreId.toString());
        }
        return genreId;
    }
}
