package ru.otus.dbarsukov.bookstore.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Author {
    private final Long id;
    private final String Name;
    private final String Surname;
}
