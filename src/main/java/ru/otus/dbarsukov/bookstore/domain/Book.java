package ru.otus.dbarsukov.bookstore.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Book {
    private final Long id;
    private final String title;
    private final Author author;
    private final Genre genre;
}
