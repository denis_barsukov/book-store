package ru.otus.dbarsukov.bookstore.cli;

import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.dbarsukov.bookstore.service.BookStoreService;

@ShellComponent
@RequiredArgsConstructor
public class ShellCommands {

    private final BookStoreService bookstore;

    @ShellMethod("Show store info")
    public void info() {
        System.out.println(bookstore.getInfo());
    }

    @ShellMethod("Print authors")
    public void authors() {
        bookstore.getAllAuthors().forEach(System.out::println);
    }

    @ShellMethod("Print genres")
    public void genres() {
        bookstore.getAllGenres().forEach(System.out::println);
    }

    @ShellMethod("Print book list")
    public void books() {
        bookstore.getAllBooks().forEach(System.out::println);
    }

    @ShellMethod(value="Add new book", key="add")
    public void addBook(String title, String genre, String name, String surname) {
        bookstore.addBook(title, genre, name, surname);
    }

    @ShellMethod(value="Find book", key="find")
    public void findBooks(@ShellOption(defaultValue="") String title,
                         @ShellOption(defaultValue="") String genre,
                         @ShellOption(defaultValue="") String name,
                         @ShellOption(defaultValue="") String surname) {
        bookstore.find(title, genre, name, surname).forEach(System.out::println);
    }
}
