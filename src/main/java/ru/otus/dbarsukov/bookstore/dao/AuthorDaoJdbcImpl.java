package ru.otus.dbarsukov.bookstore.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.dbarsukov.bookstore.domain.Author;
import ru.otus.dbarsukov.bookstore.mapper.AuthorMapper;

import java.util.HashMap;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class AuthorDaoJdbcImpl implements AuthorDao {

    private final NamedParameterJdbcOperations jdbc;

    @Override
    public long count() {
        return jdbc.getJdbcOperations()
                .queryForObject("select count(*) from authors", Long.class);
    }

    @Override
    public long create(Author entity) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", entity.getName());
        params.addValue("surname", entity.getSurname());

        final String INSERT_QUERY = "insert into authors (name, surname) " +
                "values (:name, :surname) ";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(INSERT_QUERY, params, keyHolder, new String[]{"id"});

        return keyHolder.getKey().longValue();
    }

    @Override
    public List<Author> readAll() {
        return jdbc.getJdbcOperations().query("select * from authors", new AuthorMapper());
    }

    @Override
    public long findIdByParams(String name, String surname) {
        final HashMap<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("surname", surname);

        try {
            return jdbc.queryForObject(
                    "select id from authors where name = :name and surname = :surname limit 1",
                    params, Integer.class);
        } catch (EmptyResultDataAccessException ex) {
            return -1;
        }
    }
}
