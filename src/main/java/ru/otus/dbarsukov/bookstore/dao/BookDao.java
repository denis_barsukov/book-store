package ru.otus.dbarsukov.bookstore.dao;

import ru.otus.dbarsukov.bookstore.domain.Book;

import java.util.List;

public interface BookDao{
    long create(Book entity);
    long count();
    List<Book> readAll();
    List<Book> findByParams(String title, String genre, String name, String surname);
}
