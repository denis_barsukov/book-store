package ru.otus.dbarsukov.bookstore.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.dbarsukov.bookstore.domain.Book;
import ru.otus.dbarsukov.bookstore.mapper.BookMapper;

import java.util.HashMap;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class BookDaoJdbcImpl implements BookDao {

    private final NamedParameterJdbcOperations jdbc;

    private final String SELECT_BOOKS_QUERY =
            "select * from books as b " +
                    "inner join authors as a on a.id = b.author_id " +
                    "inner join genres as g on g.id = b.genre_id ";

    @Override
    public long count() {
        return jdbc.getJdbcOperations()
                .queryForObject("select count(*) from books", Long.class);
    }

    @Override
    public long create(Book entity) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("title", entity.getTitle());
        params.addValue("authorId", entity.getAuthor().getId());
        params.addValue("genreId", entity.getGenre().getId());

        final String INSERT_QUERY= "insert into books (title, author_id, genre_id) " +
                "values (:title, :authorId, :genreId) ";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(INSERT_QUERY, params, keyHolder, new String[]{"id"});

        return keyHolder.getKey().longValue();
    }

    @Override
    public List<Book> readAll() {
        return jdbc.query(SELECT_BOOKS_QUERY, new BookMapper());
    }

    @Override
    public List<Book> findByParams(String title, String genre, String name, String surname) {
        final HashMap<String, Object> params = new HashMap<>();
        params.put("title", title);
        params.put("name", name);
        params.put("surname", surname);
        params.put("genre", genre);

        final String SELECT_BOOKS_WITH_CONDITION =
                SELECT_BOOKS_QUERY + "where title = COALESCE(NULLIF(:title, ''), title) " +
                        "and name = COALESCE(NULLIF(:name, ''), name) " +
                        "and surname = COALESCE(NULLIF(:surname, ''), surname) " +
                        "and genre = COALESCE(NULLIF(:genre, ''), genre)";

        return jdbc.query(SELECT_BOOKS_WITH_CONDITION, params, new BookMapper());
    }
}
