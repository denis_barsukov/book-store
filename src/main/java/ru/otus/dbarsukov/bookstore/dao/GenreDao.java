package ru.otus.dbarsukov.bookstore.dao;

import ru.otus.dbarsukov.bookstore.domain.Genre;

import java.util.List;

public interface GenreDao{
    long create(Genre entity);
    long count();
    List<Genre> readAll();
    long findIdByParams(String genre);
}
