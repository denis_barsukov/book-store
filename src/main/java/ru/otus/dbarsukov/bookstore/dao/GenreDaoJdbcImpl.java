package ru.otus.dbarsukov.bookstore.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.dbarsukov.bookstore.domain.Genre;
import ru.otus.dbarsukov.bookstore.mapper.GenreMapper;

import java.util.HashMap;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class GenreDaoJdbcImpl implements GenreDao {

    private final NamedParameterJdbcOperations jdbc;

    @Override
    public long count() {
        return jdbc.getJdbcOperations()
                .queryForObject("select count(*) from genres", Long.class);
    }

    @Override
    public long create(Genre entity) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("genre", entity.getGenre());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update("insert into genres (genre) values (:genre)",
                params, keyHolder, new String[]{"id"});
        return keyHolder.getKey().longValue();
    }

    @Override
    public List<Genre> readAll() {
        return jdbc.getJdbcOperations().query("select * from genres", new GenreMapper());
    }

    @Override
    public long findIdByParams(String genre) {
        final HashMap<String, Object> params = new HashMap<>();
        params.put("genre", genre);
        try {
            return jdbc.queryForObject("select id from genres where genre = :genre limit 1",
                    params, Integer.class);
        }
        catch (EmptyResultDataAccessException ex) {
            return -1;
        }
    }
}
