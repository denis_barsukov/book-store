package ru.otus.dbarsukov.bookstore.dao;

import ru.otus.dbarsukov.bookstore.domain.Author;

import java.util.List;

public interface AuthorDao{
    long create(Author entity);
    long count();
    List<Author> readAll();
    long findIdByParams(String name, String surname);
}
