drop table if exists books;
drop table if exists authors;
drop table if exists genres;

drop sequence if exists sq_authors_id;
drop sequence if exists sq_books_id;
drop sequence if exists sq_genres_id;

create sequence sq_authors_id increment by 1 start 1;
create sequence sq_books_id increment by 1 start 1;
create sequence sq_genres_id increment by 1 start 1;

create table authors(
    id        bigint default sq_authors_id.nextval primary key
  , name      varchar(255) not null
  , surname   varchar(255) not null
  , unique (name, surname)
);

create table genres(
    id        bigint default sq_genres_id.nextval primary key
  , genre     varchar(255) unique not null
);

create table books(
    id        bigint default sq_books_id.nextval primary key
  , author_id bigint references authors
  , genre_id  bigint references genres
  , title     varchar(255) not null
  , unique (author_id, title)
);
