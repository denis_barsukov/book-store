insert into genres (genre) values ('adventure novel');
insert into genres (genre) values ('science fiction');
insert into genres (genre) values ('autobiography');
insert into genres (genre) values ('fairytale');

insert into authors (name, surname) values ('Jules', 'Verne');
insert into authors (name, surname) values ('Ray', 'Bradbury');
insert into authors (name, surname) values ('Awesome', 'Author');

insert into books (title, genre_id, author_id) values ('Dandelion Wine', 2, 2);
insert into books (title, genre_id, author_id) values ('Voyage au centre de la Terre', 1, 1);
insert into books (title, genre_id, author_id) values ('Le tour du monde en quatre-vingts jours', 1, 1);
