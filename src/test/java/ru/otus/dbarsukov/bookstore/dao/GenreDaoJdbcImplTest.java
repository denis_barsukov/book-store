package ru.otus.dbarsukov.bookstore.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.dbarsukov.bookstore.domain.Author;
import ru.otus.dbarsukov.bookstore.domain.Genre;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@JdbcTest
@Import({GenreDaoJdbcImpl.class})
//@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
public class GenreDaoJdbcImplTest {

    @Autowired
    GenreDao genreDao;

    @Test
    public void count() {
        assertEquals(4, genreDao.count());
    }

    @Test
    public void create() {
        long id = genreDao.create(new Genre(null, "test Genre"));
        assertEquals(5, id);
    }

    @Test
    public void readAll() {
        List<Genre> result = genreDao.readAll();
        assertEquals(4, result.size());
    }

    @Test
    public void findIdByParams() {
        long result = genreDao.findIdByParams("fairytale");
        assertEquals(4, result);
    }
}