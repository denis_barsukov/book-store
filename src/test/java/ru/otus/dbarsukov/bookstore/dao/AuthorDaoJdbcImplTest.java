package ru.otus.dbarsukov.bookstore.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.dbarsukov.bookstore.domain.Author;
import ru.otus.dbarsukov.bookstore.domain.Book;
import ru.otus.dbarsukov.bookstore.domain.Genre;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@JdbcTest
@Import({AuthorDaoJdbcImpl.class})
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
@Transactional(propagation = Propagation.REQUIRED)
public class AuthorDaoJdbcImplTest {

    @Autowired
    AuthorDao authorDao;

    @Test
    public void count() {
        assertEquals(3, authorDao.count());
    }

    @Test
    public void create() {
        long id = authorDao.create(new Author(null, "Name1", "Surname1"));
        assertEquals(4, id);
    }

    @Test
    public void readAll() {
        List<Author> result = authorDao.readAll();
        assertEquals(3, result.size());
    }

    @Test
    public void findIdByParams() {
        long result = authorDao.findIdByParams("Ray", "Bradbury");
        assertEquals(2, result);
    }
}