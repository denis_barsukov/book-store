package ru.otus.dbarsukov.bookstore.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.dbarsukov.bookstore.domain.Author;
import ru.otus.dbarsukov.bookstore.domain.Book;
import ru.otus.dbarsukov.bookstore.domain.Genre;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@JdbcTest
@Import({BookDaoJdbcImpl.class})
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
@Transactional(propagation = Propagation.REQUIRED)
public class BookDaoJdbcImplTest {

    @Autowired
    BookDao bookDao;

    @Test
    public void count() {
        assertEquals(3, bookDao.count());
    }

    @Test
    public void create() {
        long id = bookDao.create(new Book(null, "test",
                new Author(null, "Name1", "Surname1"),
                new Genre(null, "Genre1")));
        assertEquals(4, id);
    }

    @Test
    public void readAll() {
        List<Book> result = bookDao.readAll();
        assertEquals(3, result.size());
    }

    @Test
    public void findByParams() {
        List<Book> result = bookDao.findByParams("", "adventure novel", "", "");
        assertEquals(2, result.size());
    }
}
