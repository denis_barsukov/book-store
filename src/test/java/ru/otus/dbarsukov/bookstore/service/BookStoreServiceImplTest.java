package ru.otus.dbarsukov.bookstore.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.otus.dbarsukov.bookstore.dao.AuthorDao;
import ru.otus.dbarsukov.bookstore.dao.BookDao;
import ru.otus.dbarsukov.bookstore.dao.GenreDao;
import ru.otus.dbarsukov.bookstore.domain.Author;
import ru.otus.dbarsukov.bookstore.domain.Book;
import ru.otus.dbarsukov.bookstore.domain.Genre;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookStoreServiceImplTest {

    @Mock
    private AuthorDao authorDao;
    @Mock
    private GenreDao genreDao;
    @Mock
    private BookDao bookDao;

    @Test
    public void info() {
        when(authorDao.count()).thenReturn(3L);
        when(bookDao.count()).thenReturn(3L);

        assertEquals("Books in store: 3, authors: 3",
                new BookStoreServiceImpl(authorDao, bookDao, genreDao).getInfo());

        verify(authorDao, times(1)).count();
        verify(bookDao, times(1)).count();
    }

    @Test
    public void authors() {
        List<Author> authorList = new ArrayList<>();
        authorList.add(new Author(1L, "Some", "Guy"));
        authorList.add(new Author(2L, "Awesome", "Author"));

        when(authorDao.readAll()).thenReturn(authorList);

        List<String> expectedList = new ArrayList<>();
        expectedList.add("Some Guy");
        expectedList.add("Awesome Author");

        assertEquals(expectedList,
                new BookStoreServiceImpl(authorDao, bookDao, genreDao).getAllAuthors());

        verify(authorDao, times(1)).readAll();
    }

    @Test
    public void genres() {
        List<Genre> genres = new ArrayList<>();
        genres.add(new Genre(1L, "G1"));
        genres.add(new Genre(2L, "G2"));

        when(genreDao.readAll()).thenReturn(genres);

        List<String> expectedList = new ArrayList<>();
        expectedList.add("G1");
        expectedList.add("G2");

        assertEquals(expectedList,
                new BookStoreServiceImpl(authorDao, bookDao, genreDao).getAllGenres());

        verify(genreDao, times(1)).readAll();
    }

    @Test
    public void books() {
        List<Book> books = new ArrayList<>();
        books.add(new Book(1L, "B1", new Author(1L,"N1", "S1"),
                new Genre(1L, "G1")));
        books.add(new Book(2L, "B2", new Author(2L, "N2", "S2"),
                new Genre(2L, "G2")));

        when(bookDao.readAll()).thenReturn(books);

        List<String> expectedList = new ArrayList<>();
        expectedList.add("B1 (G1) by N1 S1");
        expectedList.add("B2 (G2) by N2 S2");

        assertEquals(expectedList,
                new BookStoreServiceImpl(authorDao, bookDao, genreDao).getAllBooks());

        verify(bookDao, times(1)).readAll();
    }

    @Test
    public void find() {
        List<Book> books = new ArrayList<>();
        books.add(new Book(1L, "B1", new Author(1L,"N1", "S1"),
                new Genre(1L, "G1")));
        books.add(new Book(2L, "B2", new Author(2L, "N2", "S2"),
                new Genre(2L, "G1")));

        when(bookDao.findByParams(any(),any(),any(),any())).thenReturn(books);

        List<String> expectedList = new ArrayList<>();
        expectedList.add("B1 (G1) by N1 S1");
        expectedList.add("B2 (G1) by N2 S2");

        assertEquals(expectedList,
                new BookStoreServiceImpl(authorDao, bookDao, genreDao)
                        .find("", "G1", "", ""));
        verify(bookDao, times(1)).findByParams("","G1","","");
    }

    @Test
    public void addBook() {
        when(authorDao.findIdByParams("N1", "S1")).thenReturn(123L);
        when(genreDao.findIdByParams("G1")).thenReturn(321L);

        new BookStoreServiceImpl(authorDao, bookDao, genreDao)
                .addBook("B1", "G1", "N1", "S1");

        verify(bookDao, times(1)).create(any());
        verify(authorDao, times(1)).findIdByParams("N1", "S1");
        verify(genreDao, times(1)).findIdByParams("G1");
    }
}